#Jarod Hoeflich
#October 1, 2015
#Number Analysis

def number_analysis():
    numbers = get_list()
    make_number(numbers)

def get_list():
    print('Please Enter A Series Of 20 Random Numbers')
    list =[]
    for i in range(20):
        value =(int(input("Enter A Random Number " + str(i + 1) + ": ")))
        list.append(value)

    return list

def make_number(numbers):
    print("Lowest number: " + str(min(numbers)))
    print("Highest number: " + str(max(numbers)))
    print("Sum of the numbers: " + str(sum(numbers)))
    print("Average of the numbers:  " + str(sum(numbers)/len(numbers)))

number_analysis()