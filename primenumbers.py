# Jarod Hoeflich
# September 28th, 2015
# Prime Number

import random

def is_prime(number):
	if number <= 1:
		print("Number is not prime")
		return False
	else:
		a = 2
		while a != number:
			if number % a == 0:
				print("Number is not prime")
				return False
			a += 1
		print("Number is prime")
		return True


for number in range(1,101):
	print(number)
	is_prime(number)

question = input('Continue?')
if question == 'Yes':
	number = random.randint(1,100)
	print(number)
	is_prime(number)
if question == 'No':
	print('Goodbye')